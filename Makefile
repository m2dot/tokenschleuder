# PREFIX is environment variable, but if it is not set, then set default value
ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

tokenschleuder:
	go build

install: tokenschleuder
	install tokenschleuder $(DESTDIR)$(PREFIX)/bin/
