# Tokenschleuder

This Tool is used to manage aws cli credentials for profiles, requiring a two-factor-authentication.

## Download

* [Windows](https://bitbucket.org/m2dot/tokenschleuder/downloads/tokenschleuder.exe)
* [Mac](https://bitbucket.org/m2dot/tokenschleuder/downloads/tokenschleuder)
* [Linux](https://bitbucket.org/m2dot/tokenschleuder/downloads/tokenschleuder.x86_64)

## Installing
Either download a binary for OS from the artifacts of this repository or do the following:
```bash
> git clone .... <dir>
> cd <dir>
> make install
> tokenschleuder <args>

```

## Usage

To use it just enter your MFA Code when prompted or enter it on the command line
```bash
>	./tokenschleuder [MFA TOKEN]
```

To add a new profile that uses this Token for authentication, call tokenschleuder with the RoleArn for the new profile:
```bash
>	./tokenschleuder [ROLE ARN]
```

for example:
```bash
>	./tokenschleuder arn:aws:iam::0000000000:role/M2-Administrator
```

## Source Profile

If you use a different profile than __default__ as your MFA provider use one of the following methods:
* environment variable: TOKENSCHLEUDER_SOURCE_PROFILE
* commandline flag: -source-profile [PROFILE NAME]

### Environment Variables
The following environment variables are neccessary and should indicate the correct profiles to use
```
TOKENSCHLEUDER_SOURCE_PROFILE=default-long-term
TOKENSCHLEUDER_DESTINATION_PROFILE=default
```

## Import Siemens Energy Credentials

You can use the import statement to import siemens energy credentials:
```bash
>	./tokenschleuder import credentials.txt
```

The credentials are the ones from the Siemens Energy Token Wending Machine.
The created profiles are:
* se-sandbox
* se-acp2
* se-acp3
