// Package awsconfig implements functions for manipulating the aws cli config and credential files
package awsconfig

import (
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"gopkg.in/ini.v1"
)

// credsPath returns the path to the aws credentials file
func credsPath() (string, error) {
	specialPath := os.Getenv("AWS_SHARED_CREDENTIALS_FILE")
	if specialPath != "" {
		return specialPath, nil
	}
	currentUser, err := user.Current()
	if err != nil {
		return "", err
	}
	return filepath.Join(currentUser.HomeDir, ".aws", "credentials"), nil
}

// configPath returns the path to the aws config file
func configPath() (string, error) {
	specialPath := os.Getenv("AWS_CONFIG_FILE")
	if specialPath != "" {
		return specialPath, nil
	}
	currentUser, err := user.Current()
	if err != nil {
		return "", err
	}
	return filepath.Join(currentUser.HomeDir, ".aws", "config"), nil
}

// UpdateSession updates the fields in the [token] section in the credsFile
// if the section does not exist it creates it.
func UpdateSession(dstProfile, accessKeyId, secretAccessKey, sessionToken string) error {
	path, err := credsPath()
	if err != nil {
		return err
	}
	credFile, err := ini.Load(path)
	if err != nil {
		return err
	}

	sect := credFile.Section(dstProfile)
	if sect == nil {
		sect, err = credFile.NewSection(dstProfile)
		if err != nil {
			return err
		}
	}

	_, err = sect.NewKey("aws_access_key_id", accessKeyId)
	if err != nil {
		return err
	}
	_, err = sect.NewKey("aws_secret_access_key", secretAccessKey)
	if err != nil {
		return err
	}
	_, err = sect.NewKey("aws_session_token", sessionToken)
	if err != nil {
		return err
	}

	return credFile.SaveTo(path)
}

// UpdateProfile updates the fields in the corresponding section of the aws config file.
// if the profile is not present, UpdateProfile creates it.
func UpdateProfile(dstProfile, profile, roleArn, region, externalId string) error {
	path, err := configPath()
	if err != nil {
		return err
	}
	configFile, err := ini.Load(path)
	if err != nil {
		return err
	}
	sect := configFile.Section("profile " + profile)
	if sect == nil {
		sect, err = configFile.NewSection("profile " + profile)
		if err != nil {
			return err
		}
	}
	_, err = sect.NewKey("role_arn", roleArn)
	if err != nil {
		return err
	}
	_, err = sect.NewKey("region", region)
	if err != nil {
		return err
	}
	_, err = sect.NewKey("source_profile", dstProfile)
	if err != nil {
		return err
	}
	if externalId != "" {
		_, err = sect.NewKey("externalId", externalId)
		if err != nil {
			return err
		}
	}

	return configFile.SaveTo(path)
}

func EnsureSiemensExists() error {
	path, err := configPath()
	if err != nil {
		return err
	}

	configFile, err := ini.Load(path)
	if err != nil {
		return err
	}

	for _, account := range []struct {
		profile string
		number  string
	}{
		{profile: "se-sandbox", number: "940849821810"},
		{profile: "se-acp2", number: "643761245889"},
		{profile: "se-acp2-dev", number: "822109749593"},
		{profile: "se-acp3", number: "677066164593"},
		{profile: "se-acp3-dev", number: "531030315230"},
		{profile: "se-bkp", number: "537469806550"},
		{profile: "se-bkp-acp3", number: "019631994952"},
		{profile: "se-bkp-sandbox", number: "156525317372"},
	} {
		sect := configFile.Section("profile " + account.profile)
		if sect == nil {
			sect, err = configFile.NewSection("profile " + account.profile)
			if err != nil {
				return err
			}
		}

		_, err = sect.NewKey("region", "eu-central-1")
		if err != nil {
			return err
		}

		_, err = sect.NewKey("output", "json")
		if err != nil {
			return err
		}
	}

	return configFile.SaveTo(path)
}

func UpdateSiemensCreds(srcPath string) error {
	path, err := credsPath()
	if err != nil {
		return err
	}

	credFile, err := ini.Load(path)
	if err != nil {
		return err
	}

	sessionFile, err := ini.Load(srcPath)
	if err != nil {
		return err
	}

	profiles := []struct {
		profile string
		number  string
	}{
		{profile: "se-sandbox", number: "940849821810"},
		{profile: "se-acp2", number: "643761245889"},
		{profile: "se-acp2-dev", number: "822109749593"},
		{profile: "se-acp3", number: "677066164593"},
		{profile: "se-acp3-dev", number: "531030315230"},
		{profile: "se-bkp", number: "537469806550"},
		{profile: "se-bkp-acp3", number: "019631994952"},
		{profile: "se-bkp-sandbox", number: "156525317372"},
	}

	for _, section := range sessionFile.Sections() {
		for _, profile := range profiles {
			if !strings.Contains(section.Name(), profile.number) {
				continue
			}

			credSect := credFile.Section(profile.profile)
			if credSect == nil {
				credSect, err = credFile.NewSection(profile.profile)
				if err != nil {
					return err
				}
			}

			for _, key := range []string{"aws_access_key_id", "aws_secret_access_key", "aws_session_token"} {

				value, err := section.GetKey(key)
				if err != nil {
					return err
				}

				_, err = credSect.NewKey(key, value.String())
				if err != nil {
					return err
				}
			}
		}

	}

	return credFile.SaveTo(path)
}
