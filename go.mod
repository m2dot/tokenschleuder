module bitbucket.com/wschult/tokenschleuder

go 1.15

require (
	github.com/aws/aws-sdk-go v1.36.31
	gopkg.in/ini.v1 v1.62.0
)
