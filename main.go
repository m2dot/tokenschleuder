package main

// use go-ini
import (
	"errors"
	"flag"
	"log"
	"os"
	"strings"

	"bitbucket.com/wschult/tokenschleuder/awsconfig"
	"bitbucket.com/wschult/tokenschleuder/cliutil"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/aws/aws-sdk-go/service/sts"
)

func getDevice(devices *iam.ListMFADevicesOutput) (*iam.MFADevice, error) {
	if len(devices.MFADevices) == 0 {
		return nil, errors.New("No MFA device for default profile!")
	}
	if len(devices.MFADevices) == 1 {
		return devices.MFADevices[0], nil
	}

	options := make([]string, 0)
	for _, device := range devices.MFADevices {
		options = append(options, device.String())
	}
	index, err := cliutil.GetIndex("Choose MFA Device: ", options)
	if err != nil {
		return nil, err
	}
	return devices.MFADevices[index], nil
}

func updateToken(mfaCode, srcProfile, dstProfile string) error {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Profile:           srcProfile,
		SharedConfigState: session.SharedConfigEnable,
	}))

	iamSvc := iam.New(sess)
	stsSvc := sts.New(sess)

	devices, err := iamSvc.ListMFADevices(&iam.ListMFADevicesInput{})
	if err != nil {
		return err
	}

	device, err := getDevice(devices)
	if err != nil {
		return err
	}

	if mfaCode == "" {
		mfaCode, err = cliutil.GetInput("Enter MFA Code: ")
		if err != nil {
			return err
		}
	}

	duration := int64(129600)

	token, err := stsSvc.GetSessionToken(&sts.GetSessionTokenInput{
		TokenCode:       aws.String(mfaCode),
		SerialNumber:    device.SerialNumber,
		DurationSeconds: &duration,
	})
	if err != nil {
		return err
	}

	return awsconfig.UpdateSession(
		dstProfile,
		*token.Credentials.AccessKeyId,
		*token.Credentials.SecretAccessKey,
		*token.Credentials.SessionToken)
}

func main() {
	var err error

	srcProfile := os.Getenv("TOKENSCHLEUDER_SOURCE_PROFILE")
	dstProfile := os.Getenv("TOKENSCHLEUDER_DESTINATION_PROFILE")

	srcProfileFlag := flag.String("source-profile", "", "the profile the MFA device is associated with")
	dstProfileFlag := flag.String("destination-profile", "", "the profile the session will be written to")
	flag.Parse()
	input := flag.Arg(0)

	if *srcProfileFlag != "" {
		srcProfile = *srcProfileFlag
	}

	if *dstProfileFlag != "" {
		dstProfile = *dstProfileFlag
	}

	if srcProfile == "" {
		srcProfile = "default"
	}

	if dstProfile == "" {
		dstProfile = "token"
	}

	if input == "import" {
		path := flag.Arg(1)

		if err := awsconfig.EnsureSiemensExists(); err != nil {
			log.Fatal(err)
		}

		if err := awsconfig.UpdateSiemensCreds(path); err != nil {
			log.Fatal(err)
		}
	} else if strings.HasPrefix(input, "arn:aws:iam::") {
		roleArn := input
		profile := flag.Arg(1)
		for profile == "" {
			profile, err = cliutil.GetInput("New Profile Name: ")
			if err != nil {
				log.Fatal(err)
			}
		}
		region, err := cliutil.GetInput("Region [default: eu-central-1]: ")
		if err != nil {
			log.Fatal(err)
		}
		if region == "" {
			region = "eu-central-1"
		}
		externalId, err := cliutil.GetInput("External Id: ")
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("[+] creating new profile \"%s\"\n", profile)
		if err := awsconfig.UpdateProfile(dstProfile, profile, roleArn, region, externalId); err != nil {
			log.Fatal(err)
		}
	} else {
		mfaCode := flag.Arg(0)
		if err := updateToken(mfaCode, srcProfile, dstProfile); err != nil {
			log.Fatal(err)
		}
	}
}
